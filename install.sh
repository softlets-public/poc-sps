#! /usr/bin/env bash

curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o ./docker-compose
chmod +x ./docker-compose
