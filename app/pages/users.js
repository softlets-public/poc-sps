import Layout from "../components/Layout";
import Apollo from '../components/Apollo';
import Users from "../components/Users";
import Header from "../components/Header";

const User = props => (
  <Apollo>
    <Layout>
      <Header {...props} />
      <Users {...props} />
    </Layout>
  </Apollo>
);

export default User;
