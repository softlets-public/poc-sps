import Layout from "../components/Layout";
import Login from "../components/Login";
import Apollo from '../components/Apollo';

const Index = props => (
  <Apollo>
    <Layout>
      <Login {...props}/>
    </Layout>
  </Apollo>
);

export default Index;
