import { downloadDocuments } from "./Interfaces/api.download"

const setToken = (idToken) =>
  localStorage.setItem('id_token', idToken)

const getToken = () =>
  localStorage.getItem('id_token')

const logout = () => {
  localStorage.removeItem('id_token');
  localStorage.removeItem('user');
}

const setCurrentUser = (user) => {
  localStorage.setItem('user', JSON.stringify(user))
}

const getCurrentUser = () =>
  JSON.parse(localStorage.getItem('user'))

const formatMessage = (message) => {
  message.replace('GraphQL error:', '')
}

const downloadFile = (file) => {
  return downloadDocuments(file._id).then((response) => {
    if(response.status !== 200) {
        return;
    }
    const reader = response.body.getReader();
    return new ReadableStream({
      start(controller) {
        function push() {
          reader.read().then(({ done, value }) => {
            if (done) {
              controller.close();
              return;
            }
            controller.enqueue(value);
            push();
          });
        };
        push();
      }
    })
  }).then((stream) => {
    return new Response(stream)
  }).then((res) => res.blob()).then((blob) => URL.createObjectURL(blob)).then((url => {
    const a = document.createElement("a");
    document.body.appendChild(a);
    a.href = url;
    a.download = file.filename;
    a.click();
    window.URL.revokeObjectURL(url);
  })).catch((err) => {
    throw err;
  })
}

export {
  downloadFile,
  formatMessage,
  getCurrentUser,
  setCurrentUser,
  logout,
  getToken,
  setToken
}
