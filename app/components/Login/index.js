import { Component } from 'react';
import { graphql, compose } from "react-apollo";
import { loginMutation } from "../../Queries/queries";
import { setToken, getToken, setCurrentUser } from "../../utils";
import { formatMessage } from "../../utils";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
    };
  }
  componentDidMount() {
    const token = getToken();
    if (token) {
      this.props.url.push('/users')
    }
  }

  handleLogin = (e) => {
    e.preventDefault();
    if (!this.state.email || !this.state.password) {
      this.setState({ loginError: 'Please enter email and password' });
      return;
    }
    this.props.loginMutation({
      variables: {
        email: this.state.email,
        password: this.state.password,
      }
    }).then(response => {
      setToken(response.data.loginUser ? response.data.loginUser.token : '')
      setCurrentUser(response.data.loginUser)
      this.props.url.push('/users')
    }).catch(er => {
      this.setState({ loginError: formatMessage(er.message) })
    });
  }
  onChange = (e) => {
    this.setState({ [e.target.name]: e.target.value })
  }

  render() {
    return (
      <div className="login-bg">
        <div className="login-wrapper">
          <p className="login-header">Login</p>
          <p className="login-text">{this.state.welcomeText}</p>
          <div className="login-box">
            <div>
              <span className="login-label">Email</span>
              <input name="email" type="text" onChange={(e) => this.onChange(e)} />
              <span className="login-label">Password</span>
              <input name="password" type="password" onChange={(e) => this.onChange(e)} />
              <span className="login-error">{this.state.loginError}</span>
              <button onClick={(e) => this.handleLogin(e)}>Login</button>
            </div>
          </div>
        </div>
        <style jsx>{`
          .login-bg {
            position: relative;
            height: 100vh;
            width: 100%;
            background: linear-gradient(to top left, #041122 0%, #0b3266 100%);
            background-image: -webkit-linear-gradient(-61deg, #041122 0%, #0b3266 100%);
            background-image: -moz-linear-gradient(-61deg, #041122 0%, #0b3266 100%);
            background-image: -o-linear-gradient(-61deg, #041122 0%, #0b3266 100%);
            background-image: linear-gradient(-61deg, #041122 0%, #0b3266 100%);
          }
          .login-wrapper {
            text-align: center;
            position: relative;
            top: 10%;
            margin: 0 auto;
            width: 40%;
            height: 40%;
            background: #fff;
            padding-bottom: 10px;
            padding-top: 15px;
            box-shadow: 0px 2px 28px 0px #000000;
          }
          .login-logo {
            margin: 0 auto;
            display: flex;
            height: 36px;
          }
          .login-header {
            font-size: 24px;
            color: #0f1c37;
            margin: 10px 0 0 0;
            text-align: center;
            font-weight: bold;
          }
          .login-wrapper p:nth-child(3) {
            margin-bottom: 0;
          }
          .login-wrapper p:nth-child(4) {
            margin-top: 0;
          }
          .login-text {
            font-family: inherit;
            font-size: 16px;
            color: #0f1c37;
            text-align: center;
          }
          button {
            border-radius: 2px;
            color: #fff;
            border: none;
            height: 36px;
            width: 220px;
            font-weight: 500;
            box-shadow: 0 7px 12px -9px #0093ff;
            background-color: #0093ff;
            cursor: pointer;
          }
          .login-box {
            display: table;
            margin: 0 auto;
          }
          .login-box input[type="text"], .login-box input[type="password"] {
            border: 2px solid #eaeaea;
            background: #fff;
            padding: 8px 15px;
            font-size: 18px;
            margin-bottom: 5px;
            border-radius: 4px;
          }
          .login-box input[type="text"]:focus, .login-box input[type="password"]:focus {
            background-color: #ffffff;
            border: 2px solid #009ffa;
            border-radius: 4px;
          }
          .login-label {
            text-align: left;
            position: relative;
            font-size: 14px;
            font-weight: 500;
            color: #0f1c37;
            display: block;
            margin-bottom: 3px;
            margin-top: 15px;
          }
          .login-error {
            display: block;
            font-size: 13px;
            color: red;
            height: 20px;
            margin: 5px;
          }
        `}</style>
      </div>
    );
  }
}

export default compose(
  graphql(loginMutation, { name: "loginMutation" })
)(Login);
