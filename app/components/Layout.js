import Head from "next/head";
const Layout = props => (
  <div>
    <Head>
      <title>GraphQL-Apollo-PoC</title>
      <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet"></link>
    </Head>
    {props.children}
    <style jsx global>{`
      body { 
        font-size: 16px;
        font-family: 'Montserrat', sans-serif;
        color: #333;
        background: #eceff1;
        margin: 0;
        padding: 0;
      }
    `}</style>
  </div>
);

export default Layout;
