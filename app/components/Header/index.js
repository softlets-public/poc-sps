import { Component } from 'react';
import { getCurrentUser, logout } from '../../utils'

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showLogoutPopup: false,
      user: ''
    }
  }
  componentDidMount() {
    const user = getCurrentUser();
    this.setState({ user })
  }

  clickedLogout = () => {
    this.setState({
      showLogoutPopup: !this.state.showLogoutPopup
    });
    logout();
    this.props.url.push('/')
  }

  manageLogutPopup = () => {
    this.setState({
      showLogoutPopup: !this.state.showLogoutPopup
    });
  }

  render() {
    return (
      <div className="header-wrapper">
        <div>
          <div className="app-name">PoC app</div>
          {this.state.user &&
            <div className="user-section">
              <div className="user" onClick={this.clickedLogout.bind(this)}>
                <img src='./static/logout-icon.svg' />Logout
              </div>
            </div>
          }
        </div>
        <style jsx>{`
          .header-wrapper {
            background: #2e5bb3;
            width: 100%;
            color: #fff;
            height: 50px;
            padding: 10px 0;
            box-shadow: 0px 0px 9px 3px rgba(41,41,41,0.25);
            margin-bottom: 35px;
          }
          .app-name {
            margin: 10px 0px 0px 3%;
            font-size: 24px;
            vertical-align: middle;
            cursor: pointer;
            float: left;
          }
          .user-section{
            position: relative;
            width: 150px;
            cursor: pointer;
            float: right;
            font-size: 14px;
            margin: 15px 10px 0px 0px;
          }
          .user-section .user {
            vertical-align: middle;
            text-align: center;
            font-size: 16px;
          }
          .user-section .user img {
            display: inline-block;
            height: 15px;
            width: 20px;
            margin-right: 5px;
          }
        `}</style>
      </div>
    );
  }
}

export default Header;
