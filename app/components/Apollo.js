import { ApolloClient } from "apollo-client";
import { ApolloProvider } from "react-apollo";
import { createUploadLink } from "apollo-upload-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import fetch from 'isomorphic-unfetch';
const link = createUploadLink({ uri: "http://localhost:4000/graphql" })

if (!process.browser) global.fetch = fetch

const client = new ApolloClient({
  link,
  cache: new InMemoryCache()
});

const Apollo = props => (
  <ApolloProvider client={client}>
    {props.children}
  </ApolloProvider>
);

export default Apollo;