import { Component } from "react";
import { graphql, compose } from "react-apollo";
import fetch from "isomorphic-unfetch";
import {
  updateUserMutation,
  createUserMutation,
  listUsersQuery
} from "../../Queries/queries";
import { formatMessage } from "../../utils";

if (!process.browser) global.fetch = fetch;

class AddUser extends Component {
  constructor(props) {
      super(props);
      this.state = {
        selected: null,
        email: '',
        name: '',
        validationError: ''
      };
  }
  componentDidMount() {
    if (this.props.user) {
      this.setState({
        email: this.props.user.email,
        name: this.props.user.name
      })
    }
  }
  onChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }
  onSubmit(e) {
    e.preventDefault();
    if (this.props.user) {
      this.props.updateUserMutation({
        variables: {
          name: this.state.name,
          email: this.state.email,
          id: this.props.user.id
        },
        refetchQueries: [{ query: listUsersQuery }]
      }).then((() => {
        this.props.handlePopup();
      })).catch((err) => {
        this.setState({ validationError: formatMessage(err.message) })
      });
    } else {
      this.props.createUserMutation({
        variables: {
          name: this.state.name,
          email: this.state.email,
        },
        refetchQueries: [{ query: listUsersQuery }]
      })
      .then(res => this.props.handlePopup())
      .catch(err => {             
        this.setState({ validationError: formatMessage('Invalid input!') })
      });
    }
  }
  render() {
    return (
      <div className="form-popup">
        <form action="" className="form-container">
          <h1>{this.props.user ? 'Edit User' : 'Create User'}</h1>
          <label htmlFor="email"><b>Email</b></label>
          <input type="text"
            placeholder="Enter Email"
            name="email"
            value={this.state.email}
            onChange={this.onChange.bind(this)}
            required
          />
          <label htmlFor="name"><b>Name</b></label>
          <input
            type="text"
            placeholder="Enter Username"
            name="name"
            value={this.state.name}
            onChange={this.onChange.bind(this)}
            required
          />
            {this.state.validationError && <span className="login-error">{this.state.validationError}</span>}
            <button type="submit" className="btn" onClick={this.onSubmit.bind(this)}>{this.props.user ? 'Update' : 'Save'}</button>
            <button type="submit" className="btn cancel" onClick={this.props.handlePopup}>Cancel</button>
        </form>
        <style jsx>{`
          .form-popup {
            position: fixed;
            bottom: 35%;
            left: 40%;
            border: 3px solid #f1f1f1;
            z-index: 9;
            box-shadow: 30px 30px 30px 30px rgb(169,169,169,1);
          }
          .form-container {
            max-width: 300px;
            padding: 10px;
            background-color: white;
            -webkit-transition: 0.5s;
            overflow: auto;
            transition: all 0.3s linear;
          }
          .form-container input[type=text],
          .form-container input[type=password] {
            width: 85%;
            padding: 15px;
            margin: 5px 0 22px 0;
            border: none;
            background: #f1f1f1;
          }
          .form-container input[type=text]:focus,
          .form-container input[type=password]:focus {
            background-color: #ddd;
            outline: none;
          }
          .form-container .btn {
            background-color: #4CAF50;
            color: white;
            padding: 16px 20px;
            border: none;
            cursor: pointer;
            width: 100%;
            margin-top: 10px;
            margin-bottom:10px;
            opacity: 2;
          }
          .form-container .cancel {
            background-color: #e15151;
          }
          .form-container .btn:hover, .open-button:hover {
            opacity: 1;
          }
          .login-error { 
            font-size: 12px;
            color: red;
            margin-bottom: 1%;
          }
        `}</style>
      </div>
    );
    }
}

export default compose(
  graphql(updateUserMutation, { name: "updateUserMutation" }),
  graphql(createUserMutation, { name: "createUserMutation" }),
  graphql(listUsersQuery)
)(AddUser);