import { Component } from "react";
import { graphql, compose } from "react-apollo";
import { listUsersQuery, deleteUserMutation, fileUploadMutation, getUploadFile } from "../../Queries/queries";
import AddUser from "./AddUser"
import UploadList from './ListFile'
import {
  getToken,
} from "../../utils";

if (!process.browser) {
  global.fetch = fetch;
}

const defaultUserEmail = 'admin@example.com';

class UsersList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      showPopup: false
    };
  }
  componentDidMount() {
    if (!getToken()) {
      this.props.url.push('/')
    }
  }
  showEdit(user) {
    this.setState({ selected: user, showPopup: true })
  }
  handlePopup() {
    this.setState({ selected: null, showPopup: false })
  }
  deleteUser(id) {
    this.props.deleteUserMutation({
      variables: {
        id
      },
      refetchQueries: [{ query: listUsersQuery }]
    }).then((res => {
    }));
  }
  handleUpload({ target: { validity, files: [file] } }) {
    if (validity.valid && file) {
      this.props.fileUploadMutation({
        variables: { file },
        fetchPolicy: 'no-cache',
        refetchQueries: [{ query: getUploadFile }]
      }).then((res => {}));
    }
  }

  displayUsers() {

    return this.props.data.users.map(user => {
      if (user.email !== defaultUserEmail) {
      return (
        user && !user.password && <tr
          key={user.id}
        >
          <td>{user.name}</td>
          <td>{user.email}</td>
          <td> <span className='actions' onClick={e => {
            this.showEdit(user);
          }}><a><img src='/static/edit-icon.svg' /></a>
          </span>
            <span onClick={e => {
              this.deleteUser(user.id);
            }}>
              <a><img src='/static/delete-icon.svg' /></a>
            </span>
          </td>
          <style jsx>{`
            td {
              border: 1px solid #efefef;
              padding: 10px;
            }
            tr:nth-child(even) {
              background-color: #e0f2f1;
            }
            tr:nth-child(odd) {
              background-color: #e1f5fe;
            }
            tr:hover {
              background-color: #ededed;
            }
            img {
              height: 25px;
              width: 25px;
              cursor: pointer;
              margin-right: 20px;
            }
          `}</style>
        </tr>
      );}
    });
  }

  render() {
    return (
      <div id="main" className={this.state.showPopup ? 'back-drop' : ''}>
        <div className="list-view">
          <div className="add-user">
            <button 
              className="blue-button" 
              onClick={(e) => this.setState({ showPopup: true })}
            ><span>&#10011;</span> Add User
            </button>
          </div>
          {this.props.data.users && <table className="list">
            <tbody>
              <tr>
                <th>Name</th>
                <th>Email</th>
                <th>Actions</th>
              </tr>
              {this.displayUsers()}
            </tbody>
          </table>}
        </div>
        <div id="item2">
          {this.state.showPopup && 
            <AddUser 
              user={this.state.selected} 
              handlePopup={this.handlePopup.bind(this)} 
            />
          }
        </div><hr/>
        <label className="custom-file-upload">
          <span>&#8679; </span>Upload Document
          <input 
            type='file' 
            className="file-upload" 
            onChange={(e) => this.handleUpload(e)} 
          />
        </label>
        <UploadList {...this.props}/>
        <style jsx>{`
          .list {
            margin-left: 3%;
            margin-top: 2%;
            border-collapse: collapse;
            width: 50%;
            font-size: 14px;
            color: #000;
            font-weight: 500;
            box-shadow: 0px 0px 9px 3px rgba(41,41,41,0.1);
          }
          .list td, .list th {
            border: 1px solid #efefef;
            padding: 10px;
          }
          .list tr:nth-child(even) {
            background-color: #f2f2f2;
          }
          .list tr:hover {
            background-color: #ddd;
          }
          .list th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #0277bd;
            color: white;
          }
          .blue-button {
            cursor: pointer;
            font-size: 14px;
            font-weight: 500;
            margin-left: 3%;
            color: #fff;
            border: none;
            height: 37px;
            width: 106px;
            border-radius: 2px;
            box-shadow: 0 7px 12px -9px #0093ff;
            background-color: #0093ff; 
          }
          .blue-button:hover { 
            background-color: #0277bd; 
          }
          hr {
            margin-top: 3%;
            margin-bottom: 3%;
            border: 2px solid #ededed;
            box-shadow: 0px 0px 9px 3px rgba(41,41,41,0.1); 
          }
          input[type="file"] {
            display: none;
          }
          .custom-file-upload {
            cursor: pointer;
            font-size: 13px;
            font-weight: 600;
            border: none;
            color: #fff;
            padding: 8px 12px;
            cursor: pointer;
            width: 120px;
            text-align: center;
            margin-left: 3%;
            height: 40px;
            border-radius: 2px;
            background-color: #4CAF50;
            box-shadow: 0 7px 12px -9px #4CAF50;
          }
          .custom-file-upload:hover {
            background-color: #388e3c;
          }
        `}</style>
      </div>
    );
  }
}
export default compose(graphql(listUsersQuery),
  graphql(deleteUserMutation, { name: "deleteUserMutation" }),
  graphql(fileUploadMutation, { name: "fileUploadMutation" }),
)(UsersList);
