import React, { Component } from "react";
import { graphql } from "react-apollo";
import { getUploadFile } from "../../Queries/queries";
import {
  getToken,
  downloadFile
} from "../../utils";

if (!process.browser) {
  global.fetch = fetch;
}

class UploadList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: null,
      showPopup: false

    };
  }
  componentDidMount() {
    if (!getToken()) {
      this.props.url.push('/')
    }
  }
  download(e, file) {
    e.preventDefault();
    downloadFile(file).catch(err => {
      throw err;
    })
  }
  render() {
    return (
      <div id="main">
        <div className="list-view">
          <h4>Uploaded Files</h4>
          {this.props.data && this.props.data.fileList &&
            <ul className="list-files">
              {this.props.data.fileList.map((file, index) => {
                return (
                  <li key={index} onClick={(e) => this.download(e, file)}>
                    <img src='/static/file-icon.svg' />
                    {file.filename}
                    <img src='/static/download.svg' />
                  </li>
                );
              })}
            </ul>
          }
        </div>
        <style jsx>{`
          h4 {
            margin: 3% 0% 1% 3%; 
          }
          ul {
            width: 50%;
            padding: 0;
            margin-left: 3%;
            list-style: none; 
            box-shadow: 0px 0px 9px 3px rgba(41,41,41,0.1); 
          }
          li {
            padding-top: 0px;
            border: 0.25px solid #efefef;
            padding: 15px;
          }
          li:nth-child(even) { 
            background-color: #e0f2f1;
          }
          li:nth-child(odd) { 
            background-color: #e1f5fe;
          }
          li:hover {
             background-color: #ededed;
            cursor: pointer; 
          }
          img {
            margin-right: 5px;
            display: inline-block;
            height: 16px;
            width: 20px;
            margin-left: 6px;
           }
        `}</style>
      </div>
    );
  }
}
export default (
  graphql(getUploadFile)
)(UploadList);
