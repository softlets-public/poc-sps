import { gql } from "apollo-boost";

const listUsersQuery = gql`
  {
    users {
      id
      name
      email
    }
  }
`;
const getUploadFile = gql`
{
  fileList {
    filename
    uploadDate
    _id
  }
}
`;
const loginMutation = gql`
  mutation($email: String!, $password: String!) {
    loginUser(email: $email, password: $password) {
      email,
      token,
      name
    }
  }
`;
const createUserMutation = gql`
mutation($email: String!, $name: String!) {
  addUser(email: $email, name: $name) {
    email,
    name,
    id
  }
}
`;

const updateUserMutation = gql`
mutation($email: String, $id:ID!, $name: String) {
  updateUser(email: $email, id:$id, name: $name) {
    email,
    name
  }
}
`;

const deleteUserMutation = gql`
mutation($id:ID!) {
  deleteUser( id:$id) 
}
`;
const fileUploadMutation = gql`
mutation($file: Upload!) {
  uploadSingleFile(file: $file) 
}`;

export {
  listUsersQuery,
  getUploadFile,
  createUserMutation,
  updateUserMutation,
  deleteUserMutation,
  loginMutation,
  fileUploadMutation,
};
