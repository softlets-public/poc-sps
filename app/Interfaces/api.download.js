const downloadDocuments = async (id) => {
  const res = await fetch(`http://localhost:4000/download/${id}`)
  return res
}
export {
  downloadDocuments
}