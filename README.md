# A PoC application based on GraphQL-Apollo stack

A very basic CRUD application with User Login and Document Upload functionalities.

## Pre-requisites

- **Docker**

## Installation

Clone the repo:

```bash
git clone git@gitlab.com:softlets/poc-sps.git
cd poc-sps-module
```

Launch all services using *docker-compose* :

```bash
chmod +x install.sh
./install.sh
./docker-compose up -d --build
```
The above command launches PostgreSQL and MongoDB instances, API server and App server.

The App will be running on `http://localhost:3000`.

The API server will be running on `http://localhost:4000/graphql`.

## Login Credentials

Use the following default login details to access dashboard:

| Email  |  Password |
|---|---|
| admin@example.com  |  password |

## Tech Stack:

The app is built using the following technolgies:
- React.js as the View library
- Next.js for server-side rendering of React.js
- GraphQL as the query language
- Apollo as client/server implementation of GraphQL
- PostgreSQL for storing user data
- MongoDB for document storage
- Joi for validation

## Functionalities:

- User login
- Demonstration of basic CRUD operations such as Add, Edit, Delete users
- Display added users in table
- Upload Document
- Display the list of uploaded files
