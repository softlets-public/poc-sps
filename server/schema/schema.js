const { ApolloServer, gql } = require("apollo-server-express");
const resolvers = require('./resolvers');

var typeDefs = gql`
  type User {
    id: ID!
    name: String
    email: String
    token: String
  }
  type File {
    uploadDate: String
    filename: String!
    _id: ID!
  }
  type Query {
    user(id: ID): User
    users: [User]
    fileList: [File]
  }

  type Mutation {
    registerUser(email: String!, password: String!,name: String!): User
    addUser(email: String!,name: String!): User
    loginUser(email: String!, password: String!): User
    deleteUser(id: ID!): String!
    updateUser(id: ID!, email: String, name: String): User
    uploadSingleFile(file: Upload!): String!
  }
`;

module.exports = new ApolloServer({ typeDefs, resolvers });
