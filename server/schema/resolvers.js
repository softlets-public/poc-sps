const User = require('../models/user');
const bcrypt = require('bcrypt');
const config = require('../config/envs')
const jwt = require('jsonwebtoken');
const { GraphQLUpload } = require("apollo-server-express");
const { connect } = require('../mongodb');
const { GridFSBucket } = require('mongodb');
const { userValidations } = require('../validators/user');
const Joi = require('joi');

const resolvers = {
  Upload: GraphQLUpload,
  Query: {
    user: (root, { id }) => {
      return User.where({ id }).fetch().then(user => user && user.toJSON());
    },
    users: async root => {
      const allUsers = await User.fetchAll().then(users => users.toJSON())
      return allUsers;
    },
    fileList: async (root, ) => {
      return connect().then(async db => {
        try {
          const FILES_COLL = 'gridfsdownload.files';
          const files = await db.collection(FILES_COLL).find({}).toArray();
          return files;
        } catch (err) {
          console.log(err);
        }
      });
    },
  },
  Mutation: {
    addUser: async (root, { email, name }) => {
      await Joi.validate({ email, name }, userValidations);
      return await User.forge({
        email,
        name
      }).save().then((user) => user && user.toJSON());
    },
    registerUser: async (root, { email, password, name }) => {
        const encryptPasss = await bcrypt.hash(password, 10)
        return await User.forge({
          email,
          password: encryptPasss,
          name
        }).save().then((user) => user && user.toJSON());
    },
    loginUser: async (root, { email, password }) => {
      const user = await User.where({ email }).fetch().then(user => user && user.toJSON());
      if (!user) {
        throw new Error('Email Id is incorrect!');
      }
      const valid = await bcrypt.compare(password, user.password)
      if (!valid) {
        throw new Error('Invalid Password!');
      }
      const token = await jwt.sign(user.email, config.JWT_SECRET);
      user.token = token;
      return user
    },
    deleteUser: async (root, { id }) => {
      const user = await new User({ id })
        .destroy()
      if (!user) {
        throw new Error('Could Find the record to delete');
      }
      return 'Deleted successfully'
    },
    updateUser: async (root, data) => {
    const user = await User
      .where({ id: data.id })
      .save(data, { patch: true })
      .then(function (x) {
          return x.toJSON()
      });
      if (!user) {
        throw new Error('Could Find the record to Update!');
      }
      return user
    },
    uploadSingleFile: async (root, { file }) => {
      const { createReadStream, filename, mimetype } = await file;
      const stream = await createReadStream();
      const id = await storeFS(stream, filename, mimetype);
      return 'uploaded';
    },
  },
};

async function storeFS(stream, filename, contentType) {
  return new Promise((resolve, reject) => {
    stream.on('error', err => {
      if (stream.truncated) {}
      reject(err);
    });
    connect().then(db => {
      var bucket = new GridFSBucket(db, { bucketName: 'gridfsdownload' });
      var uploadStream = bucket.openUploadStream(filename, {contentType});
      stream
        .pipe(uploadStream)
        .on('error', err => reject(err))
        .on('finish', () => resolve({ }));
    });
  });
}

module.exports = resolvers;