const { connect } = require('./mongodb');
const { GridFSBucket, ObjectId } = require('mongodb');

module.exports = {
    downloadDocument: async (req, res) => {
        connect().then(async db => {
            const bucket = new GridFSBucket(db, { bucketName: 'gridfsdownload' });
            const FILES_COLL = 'gridfsdownload.files';
            const files = await db.collection(FILES_COLL).find({ _id: ObjectId(req.params.id) }).toArray();
            try {
                res.set('Content-Type', files[0].contentType);
                const gridfsStream = bucket.openDownloadStream(ObjectId(req.params.id));
                gridfsStream.pipe(res);
            } catch (err) {
                res.status(500).send({ message: 'Couldnot download file' })
            }
        }).catch(Err => {
            res.status(500).send({ message: 'Couldnot connect the db' })
        });
    }
}