const MongoClient = require('mongodb').MongoClient;

exports.connect = async function () {
  const client = await MongoClient.connect('mongodb://mongodb:27017', { useNewUrlParser: true })
  const db = client.db('documentsdb');
  return db;
}