var Bookshelf = require('../config/bookshelf.config');
var User = Bookshelf.Model.extend({
  tableName: 'Users'
});

module.exports = User
