const Joi = require('joi')
const userValidations = Joi.object().keys({
  name: Joi.string().alphanum().min(1).max(30).required(),
  email: Joi.string().email({ minDomainAtoms: 2 })
})
exports.userValidations = userValidations