const express = require("express");
const schema = require("./schema/schema");
const { downloadDocument } = require("./controller")
const PORT = 4000;
const app = express();
app.all('/*', function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  next();
});
app.use('/download/:id', downloadDocument)
schema.applyMiddleware({ app });
app.listen(PORT, () =>
  console.log(`GraphiQL is running on http://localhost:${PORT}/graphql`)
);
