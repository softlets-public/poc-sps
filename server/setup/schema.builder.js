const bcrypt = require('bcrypt');
const knex = require('knex')(require('../knexfile'));
knex.schema.createTable('Users', (table) => {
  table.increments('id').primary();
  table.string('email').notNullable();
  table.string('password');
  table.string('name');
  })
  .then(async () => {
    const password = await bcrypt.hash('password', 10);
    return knex('Users').insert([
      {
        email: 'admin@example.com',
        name: 'admin',
        password
      }
    ]);
  })
  .then(() => {
    knex.destroy()
  })
  .catch((err) => {
    knex.destroy();
  });
