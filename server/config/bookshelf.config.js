const knex = require('knex')(require('../knexfile'));
const Bookshelf = require('bookshelf')(knex);
Bookshelf.plugin('registry');

module.exports = Bookshelf;