const envs = require(`./${process.env.NODE_ENV||'development'}.js`);

module.exports = envs;